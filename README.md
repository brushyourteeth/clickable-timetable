Clickable Timetable instructions
=============
#### Step 1: Subject details
For each of your subjects, type the name (e.g. English) and session url* into the table at the bottom.
You can optionally add the subject code (e.g. ENE112A) and teacher's name into the table as well.
Click SAVE when complete.
**To get the lession link of a class, login to eLearn/Blackboard, enter your course, find the lesson links and right click your class' title to get the lesson link. It may start with https://elearn.eq.edu.au/webapps/bb-collaborate-bb.*
#### Step 2: Session times
Refer to your official timetable pdf for this.
Click the radio/option button to the right of the row of a subject. You can now hover over the cells of the timetable and click to add a session time for your subject.
Repeat this for each of your subjects.
You can clear a period by clicking on it until it's blank.
Click SAVE when complete.
#### Step 3: Additional settings
You can customise the background and table colours using the two colour inputs below the SAVE button.
The two checkboxes can be used to toggle showing teacher names and subject codes on your timetable.
Be sure to click BACKUP to save your data in the url in case your browser's local storage is wiped. The IMPORT button will restore your timetable if it has been backed up.